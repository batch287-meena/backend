let num = Number(prompt("Enter the number:"));

for(let i=num ; i>=0 ; i--){
	if(i <= 50){
		break;
	};

	if(i%10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};

	if(i%5 === 0){
		console.log(i);
	};
};

let word = "supercalifragilisticexpialidocious";
let consonentWord = "";
for (let i=0; i<word.length; i++){
	if(word[i] == "a"||
	 	word[i] == "e" ||
	  	word[i] == "i"||
	  	word[i] == "o"||
	    word[i] == "u"){
		continue;
	}else{
	consonentWord += word[i];
	};
}

console.log(word);
console.log(consonentWord);
