console.log("Good morning!!");

// [ SECTION ] While loop
	// A while loop takes in an expression/condition
	// If condition evaluates to true, the statements in the block will run.

	/*
	Syntax:
		while(expression/condition){
			statement;
		};
	*/

	function whileloop(){
		let count = 5;

		// While the value of count is not equal to 0
		while(count !== 0){

			// The current value of count is printed out.
			console.log("While: " + count);

			// Infinte loop, our program being written the end.
			// Make sure that expression/conditions in loops have their corresponding increment/decrement to stop the loop.
			count--;
		};
	}

	// Another example

	function gradeLevel(){
		let grade = 1;

		while(grade<=5){
			console.log("I'm a grade " + grade + " student!");
			grade++;
		};
	}

// [ SECTION ] Do While Loop
	// A do-while loop works alot like a while loop, but unlike while loops, do-while loops guarantee that the code will executed at least once.

	/*
	Syntax:
		do{
			statement
		}
		while(expression/condition)
	*/

	function doWhile(){
		let count = 20;

		do{
			console.log("Whatever happens, I will be here!");
			count--;
		}
		while(count > 0);
	};

	// Another Example
	function secondDoWhile(){
		let number = Number(prompt("Give me a number"));

		do{

			// The current value of number is printed.
			console.log('Do While: ' + number);

			// Increases the value of the number by 1 after every iteration to stop the loop when it reaches the 10 or greater
			// number = number + 1 
			number += 1;

			// provide a number of 10 or greater will run the code block once and will stop the loop.
		}
		while(number < 10);
	}

// [ SECTION ] For Loop
	// A for loop is more flexible than while and do-while loop.
	// For Loop consists of 3 parts-
		// 1. The "initialization" value that will track the progression of the loop
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not.
		// 3. The "finalExprssion" indicates how to advances the loop/how the variable will behave 

	/*
	Syntax
		for(initialization ; expression/condition ; final expression){
			statement
		}
	*/

	function forLoop(){
		for( let i = 5; i <= 20; i++){
			console.log('You atre currently: ' + i);
		};
	}

	// [ SUB-SECTION ] Loops for letters

	let myString = "alex";
	console.log(myString,"-", myString.length);

	// We create a loop that will print out the individual letters of the myString variable 
	for(let i=0; i < myString.length ; i++){

		// The current variable of myString is printed out using its index value.
		console.log(myString[i]);
	};

	// Mini-Activity
	let name=prompt("Enter your name:");

	for(let i=0; i < name.length ; i++){
		if(	name[i].toLowerCase()=="a" ||
			name[i].toLowerCase()=="e" ||
			name[i].toLowerCase()=="i" ||
			name[i].toLowerCase()=="o" ||
			name[i].toLowerCase()=="u")
		{
			console.log(0);
		}
		else {
			console.log(name[i]);
		};
	};

	console.log(name);

	// [ SECTION ] Continue and Break Statements

		// The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
		// The "break" statement is used to terminate the current loop once a match has been found

			for(let count = 0; count <= 20; count++){

				// if remainder is equal to 0
				if (count % 2 === 0){
					// Tells the code to continue to the next iteration of the loop;
					// This ignores all statements located after the continue statement;
					continue;
				};

				// The current value of number is printed out if the remainder is not equal to 0;
				console.log('Continue and Break: ' + count);

				// If the current value of count is greater than 10
				if (count > 10){

					// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20;
					// number values after 10 will no longer be printed
					break;
				};
			};

		// Another example

			let name = "alexandro";

			for (let i = 0; i <name.length; i++){
				console.log(name[i]);

				// If the vowel is equal to a, continue to the next iteration of the loop
				if (name[i].toLowerCase() === 'a'){
					console.log('Continue to the next iteration');
					continue;
				};

				// If the current letter is equal to d, stop the loop
				if(name[i] == 'd'){
					break;
				}
			}
