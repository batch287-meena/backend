console.log('Introduction to JSON (JS Object Notation)');

//[ JSON Objects ]
	// JSON stands for JavaScript Object Notation
	/* Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
	*/

	// JSON Objects
	/*
		{
			'city': 'Manila',
			'province': 'Metro Manila',
			'country': 'Philippines'
		}
	*/

//[ JSON Arrays ]

	/*
		'cities': [
			{
				'city': 'Manila City',
				'province': 'Metro Manila',
				'country': 'Philippines'
			},
		{'city': 'Quezon City', 'province': 'Metro Manila', 'country': 'Philippines'},
		{'city': 'Mumbai', 'province': 'Metro Manila', 'country': 'Philippines'}
		]
	*/

//[ JSON Methods ]
	// The JSON Object contains methods for parsing and converting data into stringified JSON

	// Converting Data into Stringified JSON
		// Stringified JSON is a JS object converted into a string to be used in other function of a JS application

	let batchesArr = [{ batchName: 'Batch X'}, {batchName: 'Batch Y'}];

	// The 'stringify' method is used to convert JS Object into a string

	console.log('Result from stringify method: \n' , JSON.stringify(batchesArr));
	console.log(batchesArr);

	let data = JSON.stringify({
		name: 'John',
		age: 31,
		address: {
			city: 'Manila',
			country: 'Philippines'
		}
	});

	console.log(data);

// [ Using Stringify Method with Variables ]
	// When information is stored in a variable and is not hard coded into an object that is beng stringified, we can supply the value with a variable.

	/* Syntax:
		JSON.stringify({
			propertyA: 'variableA',
			propertyB: 'variableB',
		})
	*/
	// Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details

	// User details
		let firstName = prompt('What is your first name?');
		let lastName = prompt('What is your last name?');
		let age = prompt('What is your age?');
		let address = {
			city: prompt('Which city do you live in?'),
			country: prompt('Which country does your city address belong to?')
		};

		let otherData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address
		});

		console.log(otherData);

// [ Converting stringified JSON into JS Objects ]

	// This happens both for sending information to a backend application and sending information back to a frontend application.

		let JSONBatches = `[{ "batchName": "Batch X"}, { "batchName": "Batch Y"}]`;

		console.log(JSONBatches);
		console.log('Result from parse method:');
		console.log(JSON.parse(JSONBatches));
		
		//MINI-ACTIVITY

		let stringifiedObject = `{"name": "John","age": 31,"address":{"city": "Manila","country": "Philippines"}}`;
		console.log('Result from parse method:');
		console.log(JSON.parse(stringifiedObject));
		console.log(stringifiedObject);