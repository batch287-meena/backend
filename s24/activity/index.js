let getCube = 2;
console.log(`The cube of ${getCube} is ${getCube**3}`);

const printAddress = (city, country, pincode) => {
	console.log(`I live at ${city}, ${country} ${pincode}.`);
};

printAddress("258 Washington Ave NW", "California", 90011);

const animal = {
	name : 'Lolong',
	species: 'saltwater crocodile', 
	weight : 1075 , 
	measurement: '20 ft 3 in'
};

const { name, speciesOfAnimal, itsWeight, itsMeasurement } = animal;

console.log(`${name} was a ${speciesOfAnimal}. He wighed at ${itsWeight} kgs with a measurement of ${itsMeasurement}.`);

let numbers= [ 1, 2, 3, 4, 5];

	numbers.forEach((number) => {
		console.log(number);
	});


const reduceNumber= numbers.reduce((x,y) => {
	return x+y;
});

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog('Frankie', 5, 'Miniature Dachshund');
console.log(myDog);
