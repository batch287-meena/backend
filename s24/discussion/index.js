console.log("JavaScript - ES6(ECMAScript) Updates [s24]")

// [ EXPONENT OPERATOR ]

	const firstNum = 8**2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

// [ TEMPLATE LITERALS ]

	// Allows us to write string without using the concatenation operator (+)

	let name = 'John';

	// Pre-Templet Literal String
		// Uses single quotes ('')
		let message = 'Hello ' + name + '! Welcome to programming!';
		console.log(message);

	// String Using Templet Literals
		// Uses backticks (``)
		message = `Hello ${name}! Welcome to programming!`;
		console.log(`Message without template literals: ${message}`);

	// Multi-line Using Tempet Literals
		const anotherMessage =`
		${name} attended a math competition.
		He won it by solving the problem 8**2 with the solution of ${firstNum}.`
		console.log(anotherMessage);

		// Template literals allows us to write string with embedded JS expressions

		const interestRate = .1;
		const principal = 1000;

		console.log(`The interest on your saving account is: ${principal * interestRate }.`);

// [ ARRAY DESTRUCTURING ]
	// Allows us to unpack elements in arrays into distinct variables 

	/* Syntax:
		let/const [ variableName, variableName, variableName ] = array;
	*/

	const fullName = [ 'Juan', 'Dela', 'Cruz' ];
	console.log(fullName[0], fullName[1], fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

	const [ firstName, middleName, lastName ] = fullName;

	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [ OBJECT DESTRUCTURING ]
	// Allows to unpack properties of objects into distinct variables

	/* Syntax:
		let/const { propertyName, propertyName, propertyName } = object;
	*/

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	// Pre-Object Destructing
		console.log(person.givenName);
		console.log(person.maidenName);
		console.log(person.familyName);

		console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to see you again!`);

	// Object Destructing
		const { givenName, maidenName, familyName } = person;

		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);

		console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you again!`);

		function getFullName ({ givenName, maidenName, familyName }){
			console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
		};

		getFullName(person);
		console.log(person);

// [ ARROW FUNCTION ]
	// Compact alternative syntax to traditional functions

	const hello = () => {
		console.log('Hello everyone!!');
	};

	hello();

	// MINI-ACTIVITY 

		const getDetails = (givenName, maidenName, familyName) => {
			console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
		};

		getDetails(person);
		getDetails(givenName, maidenName, familyName);
		getDetails('John', 'D', 'Smith');

		const students = ['John','Jane', 'Judy'];

	// Arrow Functions with loops
		// Pre-Arrow Function
		students.forEach(function(student){
			console.log(`${student} is present.`);
		});

		// Arrow Functions
		students.forEach((student) => {
			console.log(`${student} is absent.`);
		});

// [ Implicit Return Statement ]
	// There are instance when you can omit the 'return' statement

	// Pre-Arrow Function
		/*const add = (x,y) => {
			return x+y;
		};

		let total= add(1,2);
		console.log(total);*/

	// Arrow Function
		const add = (x,y) => x+y;

		let total = add(1,2);
		console.log(total);

// [ Default Function Argument Value ]
	// Provides a default argument value if none is provided when the function is involved  

	const greet = (name = 'User') => {
		return `Good morning, ${name}!`;
	};

	console.log(greet());
	console.log(greet("John"));

// [ Class-Based Project Blueprints ]
	// Allow creation/instantiation of object using classes as blueprints

	// Creating A Class
		// The constructor is a special method of a class for creating/initialising an object for that class.
		/* Syntax:
			class className{
				constructor(objectPropertyA,objectPropertyB){
					this.objectPropertyA = objectPropertyA;
					this.objectPropertyB = objectPropertyB;
				}
			}
		*/

		class Car{
			constructor(brand, name, year){
				this.brand= brand;
				this.name = name;
				this.year = year;
			}
		}

		// Initiating an object 
			// The "new" creates/intantiates a new object with the given arguments as the values of it's properties

		let myCar = new Car();

		console.log(myCar);

		myCar.brand = 'Ford';
		myCar.name = 'Mustang';
		myCar.year = 2023;

		console.log(myCar);

		const myNewCar = new Car('Toyota', 'Hilux', 2020);
		console.log(myNewCar);
		console.log(typeof myNewCar);