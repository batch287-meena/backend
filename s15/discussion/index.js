console.log("Hello World? I'm Komal")

// [SECTION] Syntax, Statements and Comments
	// JS Statements usually end with semicolon (;)
	// JS Statemnets in programming are instruction that we tell the computer/browser to perform.
	// A Syntax in programming, it is a set of rules that describes how statemnets mucst be constructed.

	/* 
	There are 2 types of comments:
		1. Single-line	//
		2. Multi-line	slash and asterisk
	*/

// [SECTION] Variables
	// It is used to contain data
	// like a storage, container
		//When we create variables, certain portions of a device's memory is given a "name" that we call "variables"

	// [ SUB-SECTION ] Declaration of Variables
	// Tells Our device/local machine that a variable name is created and is ready to store data.
	// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning that the variable's value was not yet defined. 

		//Syntax
			//let/const variableName;
		let myVariable;

		console.log(myVariable);
		// Print myVariable in the console.

		/*
		Guides in writing variables:
			1. Use the "let" keyword followed by the varible name of your choosing and use the assignment operator (=) to assign the value
			2. Varible names should start with a lowercase character, or also known for camelCase.
				Ex.
				favColor
				favNumber
				collegeCourse
			3. For constant variables, use the "const" keyword.
			4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
		*/

	// [ SUB SECTION ] Initialising Variables
		//The instance when a variable is given its initial/starting value

		// Syntax
			//let/const varibleName = value;
		let productName = "desktop computer";
		console.log(productName)

		let productPrice = 18999;
		console.log(productPrice);

		//Usually being very certain value is not changing.
		// More likely being used for interest rate for a loan, savings account or a mortgage.
		const interest = 3.539;
		console.log(interest);

	// [ SUB-SECTION ] Reassigning variable values
		//Reassigning a variable pertains to changing it's initial or previous value into another value. 

		//Syntax 
			// variable = newValue;
		productName = "Laptop";
		console.log(productName);

		//interest = 4.456;
		//console.log("Hello World?");

	// [ Var vs. Let/Const ]
		// var is also used in declaring a variable. but var is aECMAScript1 (ES1) feature. in mordern time, ES6 update are already now using let/const.

		//For example,
			a=5;
			console.log(a)
			var a;

		 // Let keyword,
			// b=5;
			// console.log(b);
			// let b;

	// [ SUB-SECTION ] let/const local/global scope 
		// Scope essentially means where this variables are available for use 
		// A block is a chunk of code bounded by {}. A blog lives in curly braces. Anything within curly braces is a block.

			let outerVarible = "hello";

			{
				var innerVarible = "hello, again";
			}

			console.log(outerVarible);
			console.log(innerVarible);

	//[ SUB-SECTION ] Multiple varible Declarations
		// Multiple varibles may be declared in one line.

		let productCode = 'DC017', productBrand = 'Dell';
		console.log(productCode, productBrand);

	//[SUB_SECTION] Data types

		//[Strings]
			//Strings are a series of characters that create a word, phrase, a sentence, or anything related to creating text. 
		//We use either single('') or double("") quotation. 

		let country = "Philippines";
		let province = "Metro Manila";

		//Concanating strings
		//Multiple strings values can be combined to create a single string using the "+" symbol.

		let fullAddress = province + "," + country;
		console.log(fullAddress);

		let greetings = "I live in the " + country;
		console.log(greetings);

		//Escape character(\) in string in combination with other characters can produce different effects 
		//"\n" refers to creating a new line in between text
		let mailAddress = 'Metro Manila\n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);
		message = 'John\'s employees went home early';
		console.log(message);

	// [Numbers]
		// Integers/whole numbers
		let headcount = 26 ;
		console.log(headcount);

		//Decibel numbers/fractions
		let grade = 98.7;
		console.log(grade);

		//Exponential notation 
		let planetDistance = 2e10;
		console.log(planetDistance);  

		//Combining text and strings 
		console.log("John's grade last quarter is " + grade);

	//[ Boolean ]
		// Values are normally used to store values related to the state of certain things 
		// This is the, true or false  

		let isMarried = false;
		let inGoodConduct = true;

		console.log("isMarried: " + isMarried);
		console.log("inGoodConduct: " + inGoodConduct);

	//[ Arrays ]
		//Add a special kind of data type that's used to store multiple values

		//Syntax 
		//let/const arrayName = [ elementA, elementB, elementC, ...]

		let grades = [ 98.7, 92.1, 90.6, 94.6 ];
		console.log(grades);

		//Different data types 
		let details = ["John", "Smith", 32, true ];
		console.log(details);

	//[ Object ]
		//Objects are other special kind of data time thats used to mimic real world objects/items

		//Syntax 
		/*let/const objectName = {
			PropertyA: value ,
			PropertyB: value 
	} */

		let person = {
			fullName: "Michael jordan",
			age: 35,
			isMarried:true,
			contact: ["12345", "67890"],
			address: {
				houseNumber: "345",
				city:"Chicago"
			}
		}

		console.log(person);

		//typeof operator is used to determine the type of data or value of a variable. It outputs a string
		console.log(typeof grades);

		// Constant objects and Arrays

		/*
		The keyword const is a little misleading.

		It does not define a constant value. Did the fines a constant reference to a value.

		Because of this you can Not:
			Reaasign the constant value
			Reaasign the constant array
			Reaasign the constant object

			But you CAN ;

			Change the elements of constant array 
			Change the properties of constant object
		*/
		const anime = [ 'one piece', 'one punch man', 'attack on titan']
		anime[0] = 'kimetsu no yaiba';

		console.log(anime);  

	//[ SECTION ] Null
		//It is used to digitally express the absence of a value in a variable declaration/initialization.
		//If we use the null values, simple means that a data type was assigned to a variable but it does not hold any value amount or is nullified.

		let spouse = null;
		let myNumber = 0;
		let myString = '';

		console.log(spouse);
		console.log(myNumber);
		console.log(myString);
