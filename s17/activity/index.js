/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function info(){
		let fullName = prompt("Enter your Full Name :");
		let age = prompt("Enter your age :");
		let location = prompt("Enter your location :");

		console.log("Hello " + fullName + "!");
		console.log("You are " + age + " old.");
		console.log("You live in " + location + ".");
	};

	info();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		console.log("1. Arijit Singh");
		console.log("2. Shreya Ghoshal");
		console.log("3. Sunidhi Chauhan");
		console.log("4. Armaan Malik");
		console.log("5. Darshan Raval");
	};

	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		console.log("1. MS Dhoni:The Untold Story");
		console.log("Rotten Tomatoes Rating: 71%");		
		console.log("2. Brahmastra");
		console.log("Rotten Tomatoes Rating: 52%");		
		console.log("3. Shershaah");
		console.log("Rotten Tomatoes Rating: 54%");		
		console.log("4. Wednesday");
		console.log("Rotten Tomatoes Rating: 71%");		
		console.log("5. Stree");
		console.log("Rotten Tomatoes Rating: 79%");


	};

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
