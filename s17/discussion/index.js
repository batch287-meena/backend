console.log("Hello! Good Morning!")

// FUNCTIONS
	// Functions in JS are lines/blocks of codes that tell our device/application/browser to perform a certain task/statement when called/invoked.

	// Function Declaration
	// Defines a function with specified parameters

	// Syntax:
	/*
		function functionName(){
			code block (-statement-)
		};
	*/

	function printName(){
		console.log("My name is Komal");
	};

	/*
		function keyword - used to defined a javascript functions functionName - the function name. Functions are named to be able to use later in the code.
		function({}) - The statements which comprises the body of the function. This is where the code to be executed.
	*/

	printName();

// Function Invocation 
	// The code blocking statements inside a function is not immediately executed when then function is defined.

		printName();
		// Invoking/Calling the functions that we declared.

// Function declaration vs function expressions 

		// Function declaration 
			// A function can be created through function declaration by using keyword and adding a function name.

		declaredFunction();  // Declared functions can be hoisted. As long as the function has been defined.

		function declaredFunction(){
			console.log("Hello World, from declaredFunction");
		};

		declaredFunction();

	// Function expressions 
		//A function expression can also be stored in a variable. This is what we called function expression.

		// Anonymous function - A function without a name.

		//variableFunction(); // Function expression cannot be hoisted.

		let variableFunction = function(){
			console.log("Hello Again!!");
		};

		variableFunction();

		let funcExpression = function funcName(){
			console.log('Hello from the other side!');
		};

		funcExpression();

		// Re-assignment declared functions 

		declaredFunction = function(){
			console.log('This is the updated declaredFunction');
		};

		declaredFunction();

 		funcExpression = function(){
 			console.log("Updated funcExpression");
 		};

 		funcExpression();

 	// Re-assign from keywords const

 		const constantFunc = function(){
 			console.log("Intialized with const!");
 		};

 		constantFunc();

 		/*
 		constantFunc = function(){
 			console.log('This is the new constantFunc');
 		}; // Result will be an error
		*/

// Function scoping 

 	// Scope is the accessibility (visibility/jurisdiction) of variables within our program

 		/*
 			JS Variables has 3 types of scope:
 				1. local/block scope
 				2. global scope
 				3. function scope
 		*/

 	{
 		let localVar= "Armando Perez";
 		console.log(localVar);
 	}

 	let globalVar= "Mr. Worldwide";

 	console.log(globalVar);
 
 	// Function scope 

 		// JS has function: Each function creates a new scope

 		function showNames(){

 			var functionVar= "Joe";
 			let functionLet= "Jane";
 			const functionConst= "John";

 			console.log(functionVar);
 			console.log(functionLet);
 			console.log(functionConst);
 		};

 		showNames();

 		var functionVar= "Mike";
 		let functionLet= "Scott";
 		const functionConst= "Den";

 		console.log(functionVar);
 		console.log(functionLet);
 		console.log(functionConst);

 	// Nested Functions
 		//You can create another function inside a function.

 		function myNewFunction(){
 			let name= "Jane";

 			function nestedFunction(){
 				let nestedName="John";
 				console.log(name);
 				console.log(nestedName);
 			};

 			nestedFunction();
 		};

 		myNewFunction();

 	// Function and Global Scoped Variables

 		// Global Scoped Variable
 		let globalName = "Alexandro";

 		function myNewFunction2(){
 			let nameInside= "Renz";

 			console.log(globalName);
 		}

 		myNewFunction2();

// Using Alert()
 	//alret() allows us to show a small window at the top of our browser page to shoe information to our users.

 		//alert() syntax:
 		// alert("<Message in the String>");

 		alert("Hello World?");

 		function showSampleAlert(){
 			alert("Hello, User! Hope you have a nice day! :)");
 		};

 		showSampleAlert();

 		console.log("I will only be log in the console when the alert is dismissed!");

 		// Notes on the use of alert();
 			// Show only an alert for short dialogues/messages to the user.
 			// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// Usimg prompt()

	// prompt() allows us to show a small window at the top of the browser to gather user input.

	let samplePrompt = prompt("Enter your name.")

	console.log("Hello, " + samplePrompt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your First Name");
		let lastName = prompt("Enter your Last Name");

		console.log("Hello " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");


			// Mini-Activity
		alert("Hello, " + firstName + " " + lastName + "!");
		alert("Welcome to my page!");

		console.log(typeof firstName);
		console.log(typeof lastName);
	};

	printWelcomeMessage();
	// Function Naming Convention
		// Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourse(){
			let courses = ['Science 101', 'Math 101', 'English 101'];
			console.log(courses);
		};

		getCourse();

		// Avoid generic names to avoid confusion within your code.

		function get(){
			let name = "Jaime";
			console.log(name);
		};

		get();

		// Avoid pointless and inappropriate function name.

		function foo(){
			console.log("Nothing!");
		};

		foo();


// Name your functions in small caps. Follow camelCase when naming variable and function.

function displayCarInfo(){
	console.log('Brand: Toyota');
	console.log('Type: Sedan');
	console.log('Price: 1,500,000 Php');
};

displayCarInfo();