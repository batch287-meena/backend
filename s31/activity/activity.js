const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200,{'content-type':'text/plain'})
		response.end('Welcome!! You are at the login page')

	}
	else {
		response.writeHead(404,{'content-type': 'text/plain'})
		response.end("Oops! Page cannot be found.");
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`)


