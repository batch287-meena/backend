const express = require('express');
const mongoose = require('mongoose');

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Setup the server
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

// Setup the Database

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.sdu1yon.mongodb.net/s36-activity?retryWrites=true&w=majority", 
		{
			useNewUrlParser:true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));




	// Server listening
	app.listen(port, () => console.log(`Currently listining to port ${port}.`));

// Add the tasks route

// Allow us to all the task routes created in "taskRoute.js" file to use "/tasks" route

	// http://localhost:1000/tasks
	app.use("/tasks", taskRoute);
