const Task = require("../models/task");

// Controller for getting all the tasks
module.exports.getAllTasks = () => {

	return Task.find({}).then( result => {
		
		return result;
	});
};

// Controller creating a task
module.exports.createTask = (requestBody) => {

	let newTask = new Task ({

		name: requestBody.name,
		status: requestBody.status
	});

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false

		} else{
			return task 
		};
	} );
};

// Controller deleting a task

module.exports.deleteTask = (taskId) =>{

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err);
			return false
		} else {
			return "Deleted Task."
		};
	});
};

module.exports.updateTask = (taskId, newContent) => {

	return Task.findByIdAndUpdate(taskId).then((updatedTask, err) => {

		if(err){
			console.log(err);
			return false
		} 

		updatedTask.name = newContent.name;
		updatedTask.status = newContent.status;

		return updatedTask.save().then((result, saveErr) => {

			if(saveErr){

				console.log(saveErr);
				return false;
			} else{
				return "task updated succesfully!"
			};
		});

		
	});
};

module.exports.getTask = (taskId) => {

	return Task.findById(taskId).then( result => {
		
		return result;
	});
};
