const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.sdu1yon.mongodb.net/s35-activity", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post( "/signup", (req, res) => {
		User.findOne({name: req.body.username}).then((result, err) => {
			if(result != null && result.username == req.body.username){

			// Return a message to the client/postman 
			return res.send("Duplicate Username found");

		} else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, saveErr) =>{
				if(saveErr){
					return console.error(saveErr);
				} else{ 
					return res.status(201).send("New user registered successfully!");

				}
			})
		}
	})

})

app.listen(port, () => console.log(`Server running at post ${port}.`));

