// Count the total number of fruits on sale 
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$count: "fruitsOnSale"
		}
	]);


// Use the count operator to count the total number of fruits with stock more than or equal to 20 
db.fruits.aggregate([
		{
			$match: { stock: { $gte: 20 } }
		},
		{
			$count: "fruitsMoreThan20"
		}
	]);

// Average price of fruits on sale per supplier 
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: { _id: "$supplier_id", averagePrice: { $avg: "$price"}}
		}
	]);

//Highest price of a fruit per supplier 
db.fruits.aggregate([
		{
			$group: { _id: "$supplier_id", highestPrice: { $max: "$price"}}
		}
	]);

// Lowest price of a fruit per supplier 
db.fruits.aggregate([
		{
			$group: { _id: "$supplier_id", lowestPrice: { $min: "$price"}}
		}
	]);