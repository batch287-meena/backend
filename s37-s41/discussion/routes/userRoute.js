const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send( resultFromController));
});

router.post("/register", (req, res) => {
	userController.registeredUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY
	router.post("/details", (req, res) => {
		userController.registeredUserDetails(req.body).then(resultFromController => res.send(resultFromController));
	})

module.exports = router;