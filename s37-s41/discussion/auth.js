// It will load our jsonwebtoken package
const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which makes it difficult to decode the information with the defined secret keyword
const secret = "CourseBookingAPI"

// [ SECTION ] JSON Web Tokens
//      JSON web token or JWT is a way of securely passing information from the server to the frontend or to other parts of the server

// Token Creation
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    // Generate a JSON Web Token using the jwt's sign method
    // Generate the token using the form data and the secret code with no additional options provided.
    return jwt.sign(data, secret, {});
}
