const express = require("express");
const mongoose = require("mongoose");

const userRoutes = require("./routes/userRoute.js"); 

const cors = require("cors");

// Create an "app" variable that stores the result of the express function
const app = express();

// Connect to Mongodb Database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.sdu1yon.mongodb.net/s37-s41?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Will use the defined port number for the application whenever the variable is available or will use port 4000 if none is defined
// Doing this syntax, it will allow flexibility when using the application locally or as a hosted applications
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});

app.use("/users", userRoutes);