const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({ email: reqBody.email }).then(result => {
		
		// The "find" method returns a record if a match is found
		if(result.length > 0){

			return true

		// No duplicate email found
		// The user is not yet registered in the database
		} else{

			return false

		}
	})
}

module.exports.registeredUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else{
			return true
		}
	})

}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches

	return User.findOne({ email: reqBody.email }).then(result => {

		// Email doesn't exist
		if(result == null){
			return false;
		} else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Correct password
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			// Wrong password
			} else{
				return false
			};
		};
	});
};

// ACTIVITY
	module.exports.registeredUserDetails = (reqBody) => {

		return User.findOne({ _id: reqBody._id }).then(result => {

			if(result == null){
				return false;
			} else{
				return result
			}
		} 
	);
};