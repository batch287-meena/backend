db.getCollection('rooms').insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description:"A simple room with all the basic necessities.",
	rooms_available: 10,
	isAvailable: false
});

db.getCollection('rooms').insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation.",
		rooms_available: 5,
		isAvailable: false 
	},
	{
		name: "family",
		accomodates: 4,
		price: 2500,
		description: "A room fit for a nuclear family going on a vacation.",
		rooms_available: 4,
		isAvailable: false 
	}
]);

db.getCollection('rooms').insertMany([
	{
		name:"queen",
		accomodates: 4,
		price: 400,
		description: "A room with a queen sized bed perfect for a simple gateway.",
		rooms_available: 15,
		isAvailable: false
	},
	{
		name:"king",
		accomodates: 4,
		price: 400,
		description: "A room with a king sized bed perfect for a simple gateway.",
		rooms_available: 10,
		isAvailable: false
	}
]);

db.getCollection('rooms').find({ name: "double" });

db.getCollection('rooms').updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_available: 0
		}
	}
);

db.getCollection('rooms').deleteMany({
	rooms_available: 0
});