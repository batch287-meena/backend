console.log("Hello World!! Good Morning!!");

//Conditional statement 
	//Allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

// [ SECTION ] If, Else If, and Else Statement

	let numA = 0;

	// IF statement
	
	if (numA<=0) {
		console.log("Hellow");
	};
	// if numA is less than 0, run console.log("Hellow");

	/*
		Syntax:
			if(condition){
				statement;
			};
	*/

	console.log(numA<=0); // Results will be true.

	// The result of expression if's conditions must result to true, else the statement inside the if() will not run.

	// Let's update the variable and run an if statement with the same condition:

	numA = -1;

	if (numA>0) {
		console.log("Hello again from numA is -1");
	};

	// This will not run because the expression now results to false:
	console.log(numA>0);

	let city = "New York"

	if (city == "New York") {
		console.log('Welcome to New York City!');
	};

	// Else If Clause

		// Executes a statement if previous conditions are false and if the specified condition is true

		let numH = 1;
		numA = 1;

		if (numA < 0) {// The condition is now false, if false code block will not run
			console.log("Hellow coz numA<0");
		}
		else if (numH > 0) { // The condition now here is true, thus this will print..
			console.log("Wurld coz numH >0");
		};

		// We were able to run the else if() statement after we evaluated that the if condition was failed.

		// If the if() condition was passed and run, we will no longer evaluate to else if() and and the process there.
		let numB = 5;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5) {
			console.log("The value is less than 5.");
		} else if(numB < 10){
			console.log("The value is less than 10.")
		};

		// Let's now update the city variable and look at another example:
		city = "Tokyo";

		if(city == "New York"){
			console.log("Welcome to New York City!");
		} else if(city == "Tokyo"){
			console.log("Welcome to Tokyo, Japan!");
		};

	// Else Statement
		// Executes a statement if all existing condition are false 

		console.log(numA, numH);
		if (numA < 0){
			console.log("Hello!");
		} else if(numH == 0){
			console.log("World");
		} else {
			console.log("Again!");
		};

		// Since both the preceding if and else if conditions failed, the else if statement was run instead.

		numB = 21;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5){
			console.log("The value is less than 5.");
		} else if(numB < 10){
			console.log("The value is less than 10.")
		} else if (numB < 15){
			console.log("The value is less than 15.")
		} else if (numB < 20){
			console.log("The value is less than 20.")
		} else {
			console.log("The value is gretater than 20!")
		};
		
		/*else{
			console.log("Do you think this will run??");
		};
		*/

		// In fact, it results to an error.

		/*else if(numH == 0){
			console.log("World");
		} else{
			console.log("How about this one??");
		};
		*/

		// Same goes for elseif, there should be preceding if first.

	// If, Else If, and Else Statement with Functions

		// Most of the times we would like to use if, else if, and else statement with functions to control the flow of our application.

		let message = 'No Message!';
		console.log(message);

		function determineTyphoonIntensity(windSpeed){

			if (windSpeed<30){
				return 'Not a typhoon yet.';
			}
			else if (windSpeed<=61){
				return 'Tropical Depression Detected.';
			} 
			else if (windSpeed>=62 && windSpeed<=88){
				return 'Tropical Storm Detected.';
			} 
			else if (windSpeed>=89 && windSpeed<=117){
				return 'Severe Tropical Storm Detected.';
			}
			else{
				return 'Typhoon detected! Keep Safe!';
			};
		};

		message = determineTyphoonIntensity(163);
		console.log(message);

	// Truthy and Falsy

		// In JS a truthy value is a value that is considered true when encountered in a boolean context

		/*
		Falsy Values/Exception for truthy:
			1. False
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. Nan
		*/ 

		if(true){
			console.log("Truthy!");
		};
		if(1){
			console.log("Truthy!");
		};
		if([]){
			console.log("Truthy!");
		};
		
		// False Example
		if(false){
			console.log("Falsy!");
		};
		if(0){
			console.log("Falsy!");
		};
		if(undefined){
			console.log("Falsy!");
		};

	// Conditional (Ternary) Operator

		// The Conditional Ternary Operator takes in three operands:
			//1. Condition
			//2. Expression to execute if the condition is truthy
			//3. Expression to execute if the condition is falsy
		// Can be used as an alternative to an "if else" statement

		/*
			Syntax:
				(expression)? ifTrue : ifFalse;
		*/

	// Single Ternary Statemnet Execution
		let ternaryResult = (1<18) ? true : false;
		//let ternaryResult = (1<18) ? false : true; -- will result to false
		console.log("Result of Ternary Operator: " + ternaryResult);

	// Multiple Statement Execution

		let name;

		function isOfLegalAge(){
			name = "John";
			return 'You are of the legal age limit';
		};

		function isUnderAge(){
			name = "Jane";
			return 'You are under the age limit';
		};

		let age = parseInt(prompt('What is your age?'));
		console.log(age, typeof age);
		let legalAge = (age > 18)? isOfLegalAge():isUnderAge();
		console.log('Result of Ternary Operator in functions: ' + legalAge + ", " + name);

	// Switch Statement

		// The switch statement evaluates an expression and matches the expression's value to a case clause.
		// The ".toLowerCase" is a function that will change the input received from the prompt into all lower case letters ensuring a match with the switch case conditions.

		/*
			Syntax:
			switch(expression){
				case value:
					statement;
					break;
				default:
					statement;
					break;
			}
		*/

		let day = prompt('What day of the week is it today?').toLowerCase();

		console.log(day);

		switch(day){
			case 'monday':
				console.log('The colour of the day is red!');
				break; 
			case 'tuesday':
				console.log('The colour of the day is blue!');
				break;
			case 'wednesday':
				console.log('The colour of the day is orange!');
				break;
			case 'thursday':
				console.log('The colour of the day is violet!');
				break;
			case 'friday':
				console.log('The colour of the day is yellow!');
				break;
			case 'saturday':
				console.log('The colour of the day is brown!');
				break;
			case 'sunday':
				console.log('The colour of the day is pink!');
				break;
			default:
				console.log('Please input a valid day!');
				break;
		}

	// Try-Catch-Finally Statement

		// Try-Catch-Finally Statement are commonly used for error handling

		function showIntensityAlert(windSpeed){
			// insert Try-Catch-Finally Statement 
			try{
				alert(determineTyphoonIntensity(windSpeed));
			}
			catch(error){
				console.log(typeof error);

				console.warn(error.message);
			}
			finally{
				alert('Intensity updates will show new alert!');
				console.log('This is from finally!');

				// Continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
			}
		}

		showIntensityAlert(56);