let http = require("http");

// Mock Database
let directory = [
		{
			"name": "Digital Electronics",
			"instructor": "Virender Singh"
		},
		{
			"name": "Power Engineering",
			"instructor": "KC"
		}
	]

http.createServer(function(request, response){

	if(request.url == '/' && request.method == 'GET'){
		response.writeHead(200, { 'content-type': 'text/plain'});
		response.end('Welcome to Booking System!');
	}

	if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200, { 'content-type': 'text/plain'});
		response.end('Welcome to your profile!');
	}

	if(request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200, { 'content-type': 'text/plain'});
		response.end("Here's our courses available");
	}

	if(request.url == '/addcourse' && request.method == 'POST'){
		let courses = '';

		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Add course to our resources');

		request.on('data', function(data){
			courses += data;
		});

		request.on('end', function(){
			courses = JSON.parse(courses);
			console.log(courses);

			let newCourse = {
				"name": courses.name,
				"instructor": courses.instructor
			};

			directory.push(newCourse);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newCourse));
			response.end();
		})
	}

	if(request.url == '/updateCourse' && request.method == 'PUT'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Update a course to our resources');
	}

	if(request.url == '/archiveCourse' && request.method == 'DELETE'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Archive courses to our resources');
	}

}).listen(4000);

console.log("Server is now successfully running at localhost:4001");