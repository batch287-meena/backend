console.log("Good Morning! JS-Array Manipulations");

// Array Methods

	// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	// Mutators Method

			// Mutator methods are functions that "mutate" or change an array after they're created

		let fruits = [ 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit' ];

		// push()
			// Adds an element in the end of an array AND returns the array's length
			// Syntax:
				// arrayName.push();

			console.log('Current array: ', fruits);
			let fruitsLength = fruits.push('Mango');
			console.log(fruitsLength);
			console.log('Muted array from push method: ', fruits);

			fruits.push('Avocado', 'Guava');
			console.log('Muted array from push method: ', fruits);

		// pop()
			// Remove the last element in an array AND returns the removed element
			// Syntax:
				// arrayName.pop();

			let removedFruit = fruits.pop();
			console.log(removedFruit)
			console.log('Mutated array from pop method: ', fruits);

		// unshift()
			// Adds one or more elements at the beginning of an array AND returns array's length
			// Syntax:
				// arrayName.unshift(elementA);
				// arrayName.unshift(elementA, elementB);

			fruits.unshift('Lime', 'Banana');
			console.log('Mutated array from unshift method: ', fruits);

		// shift()
			// Remove an element at the beginning of an array AND returns the removed element;
			// Syntax:
				// arrayName.shift();

			let anotherFruit = fruits.shift();
			console.log(anotherFruit);
			console.log('Mutated array from shift method: ', fruits);

		// splice()
			// Simultaneously remove elements from a specified index number and adds elements
			// Syntax:
				// arrayName.splice(startingIndex, deleteCount, elementToBeAdded);

			let splice = fruits.splice(1,2,'Lime', 'Cherry');
			console.log(splice);
			console.log('Muted array from splice method: ', fruits);

		// sort()
			// Rearranges the array elements in alphanumeric order
			// Syntax:
				// arrayName.sort();

			fruits.sort();
			console.log('Mutated array from sort method: ', fruits);

		// reverse()
			// Reverses the order of array elements
			// Syntax:
				// arrayName.reverse();

			fruits.reverse();
			console.log('Mutated array from reverse method: ', fruits);

	// Non-Mutators Method

			// Non-Mutator methods are functions that do not modify or change an array after they're created

			let countries = [ 'US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'IND' ];

		// indexOf()
			// Returns the index number of the first matching element found in an array
			// If no match was found, the result will be -1.
			// Syntax:
				// arrayName.indexOf(searchValue);

			let firstIndex = countries.indexOf('PH');
			console.log('Result of indexOf method: ', firstIndex);

			let invalidCountry =countries.indexOf('BR');
			console.log('Result of indexOf method: ', invalidCountry);

		// lastIndexOf()
			// Returns the index number of the last matching element found in an array
			// Syntax:
				// arrayName.lastIndexOf(searchValue);

			let lastIndex = countries.lastIndexOf('PH');
			console.log('Result of lastIndexOf method: ', lastIndex);

			// Getting the index number starting from a specified index 
			let lastIndexStart = countries.lastIndexOf('PH', 4);
			console.log('Result of lastIndexStart method: ', lastIndexStart);

		// slice()
			// Potions/slices elements from an array AND returns a new array
			// Syntax:
				// arrayName.slice(startingIndex);
				// arrayName.slice(startingIndex, endingIndex);

			// Slicing off elements from a specified index to the last element
			let slicedArrayA = countries.slice(2);
			console.log('Result of slice method: ', slicedArrayA);
			console.log(countries,'-even after slice we have all elements in array');

			// Slicing off elements from a specified index to another index
			let slicedArrayB = countries.slice(2,4); // CAN SG
			console.log('Result of slice method: ', slicedArrayB);

			// Slicing off elements starting from the last element of an array
			let slicedArrayC = countries.slice(-4);
			console.log('Result of slice method: ', slicedArrayC);

		// toString()
			// Returns an array as a string separated by commas
			// Syntax:
				// arrayName.toString();

			let stringArray = countries.toString();
			console.log('Result of toString method: ', stringArray);

		// concat()
			// Combines 2 arrays and returns the combined result 
			// Syntax:
				// arrayA.concat(arrayB);

			let taskArrayA = [ 'drink HTML', 'eat JavaScript' ];
			let taskArrayB = [ 'inhale CSS', 'breath Sass' ];
			let taskArrayC = [ 'get Git', 'be Bootstrap' ];

			let task = taskArrayA.concat(taskArrayB);
			console.log('Result of concat method: ', task);

			// Combining multiple arrays 
			let allTasks = taskArrayA.concat(taskArrayB, taskArrayB);
			console.log('Result of concat method: ', allTasks);

			// Combine arrays with elements
			let combinedTasks = taskArrayA.concat('smell Express', 'throw React');
			console.log('Result of concat method: ', combinedTasks);

		// join()
			// Returns an array as string separated by specified separator string 
			// Syntax:
				// arrayName.join('separatorString');

			let users = [ 'John', 'Jane', 'Joe', 'Robert' ];
			console.log(users.join());
			console.log(users.join(" "));
			console.log(users.join(" - "));

	// Iterations Methods
		// Iterations method are loops designed to perform repetitive tasks on arrays

		// forEach()
			// Similar to a for loop that iterates on each array element
			/* Syntax:
				arrayName.forEach(function(indivElement){
					statement
				});
			*/

			allTasks.forEach(function(task){
				console.log(task)
			});

			let filteredTasks = [];

			// Looping through All Array Items
				// It's good practice to print the current element in the console when working with array iterations

			allTasks.forEach(function(task){
				console.log(task);

				if(task.length > 10){
					console.log(task);
					filteredTasks.push(task);
				};
			});

			console.log('Result of filtered task: ', filteredTasks);

		// map()
			// Iterates on each element AND returns new array different values depending on the result of the function's operations
			// Unlike the forEach method, the map method requires the use of "return" statement in order to create another array with the performed operation
			// Syntax:
				// let/const resultArray = arrayName.map(function(indivElement))

			let numbers = [ 1, 2, 3, 4, 5 ];

			let numberMap = numbers.map(function(number){
				return number**2;
			});

			console.log('Original Array: ', numbers);
			console.log('Result of map method: ', numberMap);

		// map() vs. forEach()

			let numberForEach = numbers.forEach(function(number){
				return number**2;
			});

			console.log(numberForEach); // undefined

			// forEach(), loops over all itewms in the array as does map(), but forEach()does not return a new array.

		// every()
			//checks if all elements in an array meet the given condition
			// Returns a true value of all elements meet the condition and false if otherwise
			// Syntax:
				/* let/const resultArray = arrayName.every(function(indivElem){
					return expression/condition;
				})
				*/

			let allValid = numbers.every(function(number){
				return (number < 3);
			});

			console.log('Result of every method: ', allValid);

		// some()
			// Checks if at least one element in the array meets the condition
			// Syntax:
				/* let/const resultArray = arrayName.some(function(indivElem){
					return expression/condition;
				});
				*/

			let someValid = numbers.some(function(number){
				return (number < 2);
			});

			console.log('Some numbers in the array are greater than 2: ', someValid);

		// filter()
			// Returns a new array that contains elements which meet the given condition
			/* Syntax:
				let/const resultArray = arrayName.filter(function(indivElem){
					return expression/condition;
				}) ;

			*/

			let filterValid = numbers.filter(function(number){
				return (number < 3);
			});

			console.log('Result of filter method: ', filterValid);

			let nothingFound = numbers.filter(function(number){
				return (number = 0);
			});

			console.log('Result of filter method: ', nothingFound);

		// Filtering using forEach

			let filteredNumbers = [];

			numbers.forEach(function(number){
				console.log(number);

				if(number < 3){
					filteredNumbers.push(number);
				};
			});

			console.log('Result of filter method: ', filteredNumbers);

			let products = [ 'Mouse', 'Keyboard', 'Laptop', 'Monitor' ];

		// includes()
			// includes() method check if the argument passed can be found in the array
			/* Syntax:
				arrayName.includes(<argumentToFind>);
			*/

			let productFound1 = products.includes('Mouse');
			console.log(productFound1); //true

			productFound1 = products.includes('Headsets');
			console.log(productFound1); //false

		// Chaining Methods
			// The result of the first method is used on the second method until all 'chained'

			let filteredProducts = products.filter(function(product){
				return product.toLowerCase().includes('a');
			});

			console.log(filteredProducts);

		// reduce()
			// Evaluates elements from left to right and returns/reduce the array into a single value
			/* Syntax:
				let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
					return expression/condition;
				});
			*/

			// How the 'reduce' method works-
			/* 1. The first/result element in the array is stored in accumulator parameter
				2. The second/next element in the array is stored in the currentValue parameter
				3. An operation is performed on the 2 elements 
				4. The loop repeats step 1-3 until all elements have been accumulated 
			*/ 

			let iteration = 0;
			console.log(numbers);

			let reducedArray = numbers.reduce(function(x,y){
				console.warn('current iteration: '+ ++iteration);
				console.log('accumulator: ' +x);
				console.log('currentValue: ' +y);

				return x+y ;
			});

			console.log('Result of reduce method: ', reducedArray);

		// Reducing String Arrays
			let list = [ 'Hellow', 'Again', 'World' ];

			let reducedJoin = list.reduce(function(x,y){
				return x+' '+y;
			});

			console.log('Result of reduce method: ', reducedJoin);