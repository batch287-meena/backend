console.log("Good Morning! JS Objects(s23)");

// [ SECTION ] Objects
	// An object is a data type that is used to represent real world objects
	// Information stored in objects are represented in a "key-value" pair

		/*Syntax:
			let objectName = {
				keyA: valueA,
				keyB: valueB;
			};
		*/

		let cellphone = {
			name: 'Nokia 3210',
			manufactureDate : 1999
		};

		console.log('Creating object using object initializers/literal notation:', cellphone);
		console.log(typeof cellphone);

	// Creating object using a constructor function 
		// Creates a reusable function to create several objects that have the same data structure

		/* Syntax:
			function objectName(keyA, keyB){
				this.keyA = keyA,
				this.keyB = keyB
			}
		*/

		// This is an object
		// The "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameters

		function Laptop(keyA, keyB){
			this.name = keyA;
			this.manufactureDate = keyB;
		};

		// The "new" operator creates an instance of an object 

		let laptop = new Laptop('Lenovo', 2008);
		console.log('Result of creating object using a constructors: ', laptop);

		let myLaptop = new Laptop('MacBook Air', 2022);
		console.log('Result of creating object using a constructors: ', myLaptop);

		let oldLaptop = new Laptop('Portal R2E CCMC', 1980);
		console.log('Result of creating object using a constructors: ', oldLaptop);

		// It will return undefined without the 'new' operator
		// The behaviour for this is like calling/invoking the laptop function instead of creating a new object instance.

	// Create empty objects
		let computer = {};
		let myComputer = new Object();

		// [ SECTION ] Accessing Object Properties

		// Using the dot notation 
		console.log('Result from dot notation: ' + myLaptop.name);

		// Using the square bracket notation
		console.log('Results from square bracket notation: ' + myLaptop['manufactureDate']);

	// Accessing Array Objects
		// Accessing array elements can be also done using square brackets
		let array = [ myLaptop, oldLaptop ];
		console.log(array);

		console.log(array[0]['name']);

		// This tells us that array is an object by using the dot notation
		console.log(array[1].manufactureDate);

	// Initializing/Adding object properties using dot notation

		let car = {};

		car.name = 'Honda Civic';
		console.log('Result from Adding Object Properties using dot notation: ', car);

	// Initializing/Adding object properties using bracket notation

		car['manufacture date'] = 2023;
		console.log(car['manufacture date']);
		console.log(car['manufacture Date']); //undefined
		console.log(car.manufacturedate); //undefined
		console.log('Result from Adding Object Properties using bracket notation: ', car);

	// Deleting object properties 
		delete car['manufacture date'];
		console.log('Result from deleting properties: ', car);

	// Reassigning object properties 
		car.name = 'Dodge Charger R/T';
		console.log('Result from reassigning properties: ', car);

// [ SECTION ] Object Methods
	// A method is a function which is property of an object

		let person = {
			name: 'John',
			talk: function(){
				console.log('Hello my name is ' + this.name);
			}
		};

		console.log(person);
		console.log('Result from object method:');
		person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

		let friend = {
			firstName: 'Joe',
			lastName: 'Smith',
			address: {
				city: 'Austine',
				country: 'Texas'
			},
			email: ['joe@mail.com', 'joesmith@mail.xyz'],
			introduce: function(){
				console.log('Hello my name is ' + this.firstName + " " + this.lastName);
			},
			introduceAddress: function(){
				console.log('I currently live in ' + this.address.city + ", " + this.address.country);
			},
			introduceContactDetails: function(){
				console.log('My email address follows: ' + this.email);
			}
		};

		friend.introduce();
		friend.introduceAddress();
		friend.introduceContactDetails();

// [ SECTION ] Real World Application of Objects

		/*
		Scenario
			1. We would like to create a game that would have several Pokemon interact with each other 
			2. Every Pokemon would have the same set of stats, properties and functions
		*/

	// Using objects literals to create multiple kinds of Pokemon would be time consuming

		let myPokemon = {
			name: 'Pikachu',
			level: 3,
			health: 100,
			attack: 50,
			tackle: function(){
				console.log('The Pokemon tagged targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
			},
			faint: function(){
				console.log('Pokemon fainted!');
			}
		}

		console.log(myPokemon);

		function Pokemon(name, level){

			// Properties
			this.name = name;
			this.level = level;
			this.health = 2*level;
			this.attack = level;

			// Methods
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);
				console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
			};
			this.faint = function(){
				console.log(this.name + ' fainted ');
			};
		};

		let pikachu = new Pokemon("Pikachu", 16);
		let rattata = new Pokemon("Rattata", 8);

		pikachu.tackle(rattata);
		rattata.faint();