// TRAINER
	let trainer = {
		name: "Ash Ketchun",
		age: 10,
		pokemon: [ 'Pikachu', 'Charized', 'Squitle', 'Bulbasaur'],
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		talk: function(){
			console.log('Pikachu! I choose you!')
		}
	}

	console.log(trainer);
	console.log('Result of dot notation: ', trainer.name);
	console.log('Result of sqaure bracket notation: ', trainer['pokemon']);
	console.log('Result of talk method:');
	trainer.talk();

// CONSTRUCTOR
	function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;
		this.faint = function(targetPokemon){
			console.log(targetPokemon.name + ' has fainted.')
		};
		this.tackle = function(targetPokemon){
				targetPokemon.health -= this.attack;
				console.log(this.name + " tackled " + targetPokemon.name + "!");
				console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health + ".");
				if(targetPokemon.health <= 0){
					this.faint(targetPokemon);
				}
		};
		
	};

	let pikachu = new Pokemon('Pikachu', 12);
	let geodude = new Pokemon('Geodude', 8);
	let mewtwo = new Pokemon('Mewtwo', 200);

	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	console.log(geodude);


