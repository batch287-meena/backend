console.log("Hello World! Good morning!")

// [ SECTION ] Functions

	//[ SUB-SECTION ] Parameters and Arguments

		// Functions in JS are lines/blocks of codes that tell our devices/application/browser to perform a certain task when called/invoked/triggered.

		function printInput(){
			let nickname = prompt("Enter your nickname:");
			console.log("Hi, " + nickname);
		};

		//printInput();

		// For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

		// name only acts like a variable
		function printName(name){

		// This one should work as well:
			// let putName = name;
			// console.log("Hi, " + putName);

			console.log("Hi, " + name);
		};

		// console.log(name) is error as it is not defined.

		printName("Juana"); // "Juana" is argument.

		// You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

		printName("Aaniya");

		// When the "printName()" function is called again, it stores the value of "Aaniya" in the parameter "name" then uses it to print a message.

		// Variables can also be passed as an argument.
		let sampleVariable = "Yui";

		printName(sampleVariable);

		// Function argumnets cannot be used by a function if there are no parameters provided within the function.

		printName(); // It will be Hi, undefined

		function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is: " + remainder);
			let isDivisibleBy8 = remainder ===0;
			console.log("Is " + num + " divisible ny 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(3482952);
		checkDivisibilityBy8(88);

		// You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Arguments

		// Function parameters can also accept other function as arguments 

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.")
		};

		function invokeFunction(iAmNotRelated){
			iAmNotRelated();
			argumentFunction();
		};

		invokeFunction(argumentFunction);

		// Adding and removing the parentheses "()" impacts the output of JS heavily.

		console.log(argumentFunction);
		// Will provide information about a function in the console using console.log();

	// Using multiple parameters

		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName +" "+ middleName +" "+ lastName)
			console.log(middleName +" "+ lastName +" "+ firstName)
		};

		createFullName("Mike", "Kel", "Jordan");

		// In JS, providing more/less arguments than the expected parameters will it return an error.

		// Using Variable as Arguments

		let firstName = "Dwayne";
		let secondName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName); 

// [ SECTION ] The Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			console.log("This message is from console.log");
			return firstName + " " + middleName + " " + lastName;
			console.log("This message is from console.log");
		};

	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	// In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

	// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statment is ignored because it ends the function execution.

	// In this example, console.log will print the returned value of the returnFullName() function.
	console.log(returnFullName(firstName, secondName, lastName));

	// You can also create a variable inside the function to contain the result and return that variable instead.

	function returnAddress(city, country){
		let fullAddress= city + ", "+ country;
		return fullAddress;
	};

	let myAddress = returnAddress("Manila City", "Philippines");
	console.log(myAddress);

	// On the other hand, When a function only has console.log() to display its result will be undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	};

	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1);

	// Returns undefine because printPlayerInfo return nothing. It only console.log the details.

	// You cannot save any value from printPlayerInfo() because it does not return anything.





