console.log("Hello world")

// [ SECTION ] Arithmetic Operators

	let x = 1_397;
	let y = 7_831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x/y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// [ SECTION ] Assignment Operators (=)

	//Basic Assignment Operator (=)
	//This assignment operator assigns the value of the right operand to a variable and assigns the result to the variable 
	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	//Shorthand for assignmentNumber = assignmentNumber += 2;
	assignmentNumber += 2;
	console.log("Result of shorthand addition assignment operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division assignment operator ( -=, *=, /= , %=, **=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);	

	// Multiple Operators and Parentheses 

		//PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction)

		let mdas = 1+2-3*4/5;
		console.log("Result of mdas assignment operator: " + mdas);

		let pemdas = 1 + (2-3) * (4/5);
		console.log("Result of pemdas operator: " + pemdas);

// [ SECTION ] Increment and Decrement 
		// Operators that add or subtract values by one and resigns the value of the variable where the increment/decrement was applied to

		let z=1;

		let increment = ++z;
		console.log("Result of pre-increment : " + increment);
		console.log("Result of pre-increment : " + z);

		increment = z++;
		// The value of "z" is at 2 before it was incremented
		console.log("Result of post-increment : " + increment);
		//The value of "z" was increased again reassigning the value to 3
		console.log("Result of post-increment : " + z);

		let decrement = --z;
		console.log("Result of pre-decrement : " + decrement);
		console.log("Result of pre-decrement : " + z);

		decrement = z--;
		console.log("Result of post-decrement : " + decrement);
		console.log("Result of post-decrement : " + z);

// [ SECTION ] Type Coercion
		// Is the automatic or implicit conversion of values from one data type to another.

		let numA= 10;
		let numB= 12;

		let coercion = numA + numB;
		console.log(coercion, typeof coercion, typeof numA, typeof numB);
		
		// Black text means that the output returned is a string data type.

		let numC= 16, numD= 14, nonCoercion = numC + numD;
		
		console.log(nonCoercion, typeof nonCoercion);

		let numE= true + 1, numF= false + 1;
		console.log(numE, numF, typeof numE, typeof numF);
		// true = 1 .... false = 0

// [ SECTION ] Comparision Operator

		let juan = 'juan';

		//Equality operator
		/* 
			- Checks whether the operands are equal/have the same content
			- Attempts to CONVERT and COMPARE
			- Returns a boolean value
		*/

		console.log(1==1, 1==2 , 1=='1', 0 == false, 1 == true, 'juan'=='juan', 'juan'==juan );

		//Inequality operator (!=)
		/*
			- Check whether the operants are not equal/have different content
			- Attempts to CONVERT and COMPARE operands of different data types
		*/
		console.log(1!=1, 1!=2 , 1!='1', 0 != false, 1 != true, 'juan'!='juan', 'juan'!=juan );

		// Strict Equality Operator(===)
		/*
		- Test whether the operands are equal/have the same content
		- Also COMPARES the data types of 2 values
		- Equality operators are better to use in most cases to ensure the data types provided are correct
		*/

		console.log( 1===1,
					 1===2,
					 1==='1',
					 0===false,
					 1===true,
					 'juan'==='juan',
					 'juan'===juan
					 ) 

		// Strict Inequality Operator(!==)		
		/*
		- Test whether the operands are not equal/have the same content
		- Also COMPARES the data types of 2 values
		*/
		console.log( 1!==1,
					 1!==2,
					 1!=='1',
					 0!==false,
					 1!==true,
					 'juan'!=='juan',
					 'juan'!==juan
					 )

// [ SECTION ] Relational Operators
		//Some comparison operators check whether one value is greater or less than to the other value.

		let a=50, b=65, isGreaterThan= a>b, isLessThan= a<b, GTorEqual= a>=b , LTorEqual= a<=b;

		//GT or Greater Than Operator (>)
		//LT or Less Than Operator (>)
		//GTE or Greater Than or Equal Operator (>)
		console.log(isGreaterThan, isLessThan, GTorEqual, LTorEqual);

// Logical Operators (&&- double ampersand, ||- double pipe)
		let isLegalAge= true, isRegistered= false,
		allRequirementsMet= isLegalAge && isRegistered,
		someRequirementsMet= isLegalAge || isRegistered,
		someRequirementsNotMet= !isRegistered;

		console.log("Result of Logical AND Operator " + allRequirementsMet);
		console.log("Result of Logical OR Operator " + someRequirementsMet);
		console.log("Result of Logical NOT Operator " + someRequirementsNotMet);